import QtQuick 2.4
import Qt.labs.settings 1.0
import Ubuntu.Components 1.3
import Ubuntu.Web 0.2
import Ubuntu.Components.Popups 1.3
import com.canonical.Oxide 1.0 as Oxide

import "Components" as Components

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'anilist.bhdouglass'

    automaticOrientation: true

    width: units.gu(50)
    height: units.gu(75)

    Settings {
        id: settings
        property string lastUrl: 'https://anilist.co'
        property string username
    }

    Page {
        id: page
        anchors {
            fill: parent
            bottom: parent.bottom
        }
        width: parent.width
        height: parent.height

        header: PageHeader {
            id: header
            visible: false
        }

        WebContext {
            id: webcontext
            userAgent: 'Mozilla/5.0 (Linux; Android 7.1.2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Mobile Safari/537.36 Ubuntu Touch Webapp'
            userScripts: [
                Oxide.UserScript {
                    context: 'oxide://main-world'
                    emulateGreasemonkey: true
                    url: Qt.resolvedUrl('inject.js')
                }
            ]
        }

        WebView {
            id: webview
            anchors {
                top: parent.top
                bottom: nav.top
            }
            width: parent.width
            height: parent.height

            context: webcontext
            url: settings.lastUrl
            onUrlChanged: {
                var strUrl = url.toString();
                if (settings.lastUrl != strUrl && strUrl.match('(http|https)://anilist.co/(.*)')) {
                    settings.lastUrl = strUrl;
                }
            }
            preferences.localStorageEnabled: true
            preferences.appCacheEnabled: true

            function navigationRequestedDelegate(request) {
                var url = request.url.toString();
                var isvalid = false;

                if (!url.match('(http|https)://anilist.co/(.*)')) {
                    Qt.openUrlExternally(url);
                    request.action = Oxide.NavigationRequest.ActionReject;
                }
            }

            Component.onCompleted: {
                preferences.localStorageEnabled = true;
            }
        }

        ProgressBar {
            height: units.dp(3)
            anchors {
                left: parent.left
                right: parent.right
                top: parent.top
            }

            showProgressPercentage: false
            value: (webview.loadProgress / 100)
            visible: (webview.loading && !webview.lastLoadStopped)
        }

        Components.BottomNavigationBar {
            id: nav

            function animeList() {
                if (settings.username) {
                    return 'https://anilist.co/user/' + settings.username + '/animelist';
                }

                return null;
            }

            function mangaList() {
                if (settings.username) {
                    return 'https://anilist.co/user/' + settings.username + '/mangalist';
                }

                return null;
            }

            selectedIndex: -1 // Don't show any items as active
            model: [
                {
                    'name': i18n.tr('Home'),
                    'iconName': 'home',
                    'url': 'https://anilist.co',
                },
                {
                    'name': i18n.tr('Anime List'),
                    'iconName': 'view-list-symbolic',
                    'url': animeList(),
                },
                {
                    'name': i18n.tr('Manga List'),
                    'iconName': 'view-list-symbolic',
                    'url': mangaList(),
                },
                {
                    'name': i18n.tr('Search'),
                    'iconName': 'toolkit_input-search',
                    'url': 'https://anilist.co/search/anime',
                },
                {
                    'name': i18n.tr('Settings'),
                    'iconName': 'settings',
                    'url': null,
                }
            ]

            onTabThumbClicked: {
                if (model[index].url) {
                    webview.url = model[index].url;
                }
                else {
                    PopupUtils.open(settingsComponent, root, {
                        username: settings.username,
                    });
                }
            }
        }
    }

    Component {
        id: settingsComponent

        Dialog {
            id: settingsDialog
            text: i18n.tr('Settings')

            property alias username: user.text

            function save() {
                settings.username = user.text;
                PopupUtils.close(settingsDialog);
            }

            Label {
                text: i18n.tr('Username')
            }

            TextField {
                id: user
                width: parent.width
                inputMethodHints: Qt.ImhNoPredictiveText | Qt.ImhNoAutoUppercase

                onAccepted: settingsDialog.save()
            }

            Button {
                text: i18n.tr('OK')
                color: UbuntuColors.green

                onClicked: settingsDialog.save()
            }
        }
    }

    Connections {
        target: UriHandler
        onOpened: {
            webview.url = uris[0];
        }
    }

    Component.onCompleted: {
        if (Qt.application.arguments[1] && Qt.application.arguments[1].indexOf('anilist.co') >= 0) {
            webview.url = Qt.application.arguments[1];
        }
    }
}
